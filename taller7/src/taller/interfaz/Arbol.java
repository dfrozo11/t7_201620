package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

import org.json.simple.JSONObject;

import com.google.gson.JsonObject;




public class Arbol implements IReconstructorArbol
{

	private String []  preOrden;

	private String[] inOrden;

	private BufferedReader br;

	private Nodo raiz;

	public void cargarArchivo(String nombre) throws IOException
	{
		br = new BufferedReader(new FileReader("./data/"+nombre));

		String actual = br.readLine();

		int n=1;
		while(actual!=null)
		{
			if( n==1)
			{
				String[] subdividir =actual.split("preorden=");
				preOrden = subdividir[1].split(",");
				actual=br.readLine();
				n++;
			}
			else
			{
				String[] subdividir =actual.split("inorden=");
				inOrden= subdividir[1].split(",");			
				actual=br.readLine();
				n++;
			}
		}
	}

	public void crearArchivo(String info) 
	{

		FileWriter nuevo;
		try {
			nuevo = new FileWriter("./data/arbolPlantado.json");
			nuevo.write(info);
			nuevo.flush();
			nuevo.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	@Override
	public void reconstruir() 
	{
		raiz = reconstruir(preOrden, inOrden);
		JSONObject root = new JSONObject();
		crearArchivo(generarJSON(raiz, root).toJSONString());
	}


	private JSONObject generarJSON(Nodo nodo, JSONObject j)
	{
		if (nodo==null) {
			JSONObject x = new JSONObject();
			x.put("valor","nulo" );
			return x ;
		}

		j.put("valor",nodo.darValor() );
		JSONObject innerizq = new JSONObject();
		JSONObject innerder = new JSONObject();
		j.put("hijo izquierdo",generarJSON(nodo.getLeft(), innerizq));
		j.put("hijo derecho",generarJSON(nodo.getRigth(), innerder));


		return	j;
	}

	public Nodo reconstruir(String[] arreglo, String[] orden)
	{

		if (orden.length==1) return new Nodo<String>(orden[0]);
		if(orden.length==0)return null;

		String linea = "";
		Nodo<String> pNodo = null;
		String ordenado="";
		for( int i=0; i< orden.length;i++)
		{
			String coma = ",";
			linea+=orden[i]+coma;
		}

		for( int i=0; i<arreglo.length;i++)
		{
			if(linea.contains(arreglo[i]))
			{
				pNodo= new Nodo<String>(arreglo[i]);
				break;
			}

		}


		String[] derecha =  linea.split( pNodo.darValor())[1].split(",");
		String []izquierda =linea.split( pNodo.darValor())[0].split(",");
		pNodo.setLeft(reconstruir(arreglo, izquierda));
		pNodo.setRigth(reconstruir(arreglo, derecha));

		return pNodo;

	}


	public boolean contieneSubarbol(Nodo raizSub)
	{
		String recInOr =recorridoInOrden(raizSub).trim();
		String recPreOr =recorridoPreOrden(raizSub).trim();
		
		String arbolACInOrden = recorridoInOrden(raiz).trim();
		String arbolACPreOrden = recorridoPreOrden(raiz).trim();

		if(arbolACInOrden.contains(recInOr)&&arbolACPreOrden.contains(recPreOr))
		{
			return true;
		}

		return false;
	}


	public String recorridoPreOrden(Nodo actual)
	{
		if(actual==null)return "";
		String resp = actual.darValor()+" ";

		resp+=recorridoInOrden(actual.getLeft());
		resp+=recorridoInOrden(actual.getRigth());

		return resp;

	}

	public String recorridoInOrden(Nodo actual)
	{
		if(actual==null)return "";
		String resp=recorridoInOrden(actual.getLeft());
		resp += actual.darValor()+" ";
		resp+=recorridoInOrden(actual.getRigth());

		return resp;

	}
	
	public Nodo darRaiz()
	{
		return raiz;
	}
	
	public void Raiz(Nodo Raiz)
	{
		raiz =Raiz;
	}

} 
