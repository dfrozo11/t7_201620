package taller.interfaz;

public class Nodo <k>
{
	private k valor;

	private Nodo izquierdo;
	private Nodo derecho;

	public Nodo(k pValor)
	{
		valor =pValor;
	}

	
	public void setLeft(Nodo pNext)
	{
		izquierdo=pNext;
	}

	public Nodo getLeft()
	{
		return izquierdo;
	}
	

	public void setRigth(Nodo pNext)
	{
		derecho=pNext;
	}

	public Nodo getRigth()
	{
		return derecho;
	}
	public k darValor()
	{
		return valor;
	}

}
