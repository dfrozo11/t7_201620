package taller.test;

import java.io.IOException;

import junit.framework.TestCase;
import taller.interfaz.Arbol;

public class MainTest extends TestCase{


	private Arbol arbol;

	private Arbol subArbol;

	private String[] preorden;

	private String[] inorden;

	private void setupscenario1()
	{
		arbol = new Arbol();
		preorden = new String[7];
		preorden[0]="a";
		preorden[1]="b";
		preorden[2]="d";
		preorden[3]="e";
		preorden[4]="c";
		preorden[5]="f";
		preorden[6]="g";
		inorden= new String[7];
		inorden[0]="d";
		inorden[1]="b";
		inorden[2]="e";
		inorden[3]="a";
		inorden[4]="f";
		inorden[5]="c";
		inorden[6]="g"; 

	}

	private void setupscenario2()
	{
		arbol = new Arbol();
		preorden = new String[7];
		preorden[0]="j";
		preorden[1]="k";
		preorden[2]="l";
		preorden[3]="o";
		preorden[4]="q";
		preorden[5]="h";
		preorden[6]="p";
		inorden= new String[7];
		inorden[0]="k";
		inorden[1]="l";
		inorden[2]="j";
		inorden[3]="h";
		inorden[4]="q";
		inorden[5]="o";
		inorden[6]="p"; 

	}
	
	private void setupScenario3()
	{
		arbol= new Arbol();
		subArbol= new Arbol();
		try {
			arbol.cargarArchivo("nombre.properties");
			subArbol.cargarArchivo("subarbol.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testReconstruccion()
	{

		setupscenario1();

		arbol.Raiz(arbol.reconstruir(preorden, inorden));

		try
		{

			assertNotNull("EL arbol no genero ninguna raiz", arbol.darRaiz());

			assertEquals("EL arbol no se contruyo correctamente", "a", arbol.darRaiz().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "b", arbol.darRaiz().getLeft().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "c", arbol.darRaiz().getRigth().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "d", arbol.darRaiz().getLeft().getLeft().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "e", arbol.darRaiz().getLeft().getRigth().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "f", arbol.darRaiz().getRigth().getLeft().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "g", arbol.darRaiz().getRigth().getRigth().darValor());
			
		}
		catch( Exception e)
		{
			assertTrue("Alguno de los hijos no se creo correctamente", false);
		}

		setupscenario2();
		arbol.Raiz(arbol.reconstruir(preorden, inorden));
		try
		{

			assertNotNull("EL arbol no genero ninguna raiz", arbol.darRaiz());

			assertEquals("EL arbol no se contruyo correctamente", "j", arbol.darRaiz().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "k", arbol.darRaiz().getLeft().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "o", arbol.darRaiz().getRigth().darValor());
			System.out.println( arbol.darRaiz().getLeft().getLeft().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "l", arbol.darRaiz().getLeft().getRigth().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "q", arbol.darRaiz().getRigth().getLeft().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "p", arbol.darRaiz().getRigth().getRigth().darValor());
			assertEquals("EL arbol no se contruyo correctamente", "h", arbol.darRaiz().getRigth().getLeft().getLeft().darValor());

		}
		catch( Exception e)
		{
			assertTrue("Alguno de los hijos no se creo correctamente", false);
		}

	}
	
	
	public void testSubArbol()
	{
		setupScenario3();
		assertTrue("no se detecto el subarbol dado", arbol.contieneSubarbol(subArbol.darRaiz()));
	}
	
	

}